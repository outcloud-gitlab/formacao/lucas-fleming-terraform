![Findmore Academy - Formação GitLab CI/CD](./formacao.png "Formação GitLab CI/CD")

Este é um repositório base com código de Terraform, que será usado como conteúdo inicial para os trabalhos com pipelines no GitLab CI/CD.

---
---

## Conteúdo do Repositório

O código do Terraform neste repositório cria os seguintes recursos:

| Tipo do Recurso | Bloco do Terraform | Documentação Terraform |
| --- | --- | --- |
| Resource Group | azurerm_resource_group.this | [Documentação azurerm_resource_group](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group) |
| Storage Account | azurerm_storage_account.this | [Documentação azurerm_storage_account](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/storage_account) |

---
---

## Documentação GitLab

- [.gitlab-ci.yml Keyword Reference](https://docs.gitlab.com/ee/ci/yaml/)
- [GitLab Predefined Variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- [GitLab CI/CD Variable Precedence](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence)
- [Difference Between Cache and Artifacts](https://docs.gitlab.com/ee/ci/caching/)
- [Choose When to Run Jobs](https://docs.gitlab.com/ee/ci/jobs/job_control.html)

---
---

## Documentação Azure

- [Regiões da Azure](https://learn.microsoft.com/en-us/azure/reliability/availability-zones-service-support#azure-regions-with-availability-zone-support)